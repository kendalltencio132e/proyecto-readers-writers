/* Fichier: LecteurEgoiste.cpp Auteur: Kendall Tencio */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <semaphore.h>
#include <mutex>
#include <string.h>
#include "lesStructures.hpp"

using namespace std;

#define MAX_EGOISTE 3;

struct lecteur_egoiste_thread_donnees
{
    int num_actualle_egoiste = 0;
};

struct lecteur_egoiste_donnees
{
    int quantite;
    int temps_dormi;
    int temps_lu;
    int ma_ligne;
    int faussesIdentites;
};

executionEtats *moi_etats;
partagees_donnees *pd;
lecteur_egoiste_donnees led;
lecteur_egoiste_thread_donnees letd;

int *maxlignes;
sem_t *semsf;
sem_t *sempr;
mutex mtx;
mutex mtx_cout;

void processusGenerateur();

bool egoistePermitiur;
bool etatFinalise = false;

void enregistrerDesDonness(string donness, int *seek)
{
    int file = open(FILENAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    lseek(file, *seek, SEEK_CUR);
    *seek += write(file, &(donness[0]), donness.length());
    close(file);
}

string obtenirTemps(bool inclureDate)
{
    time_t now = time(0);
    tm *ltm = localtime(&now);
    string date = to_string(ltm->tm_mday) + "/" + to_string(1 + ltm->tm_mon) + "/" + to_string(1900 + ltm->tm_year);
    string heure = to_string(ltm->tm_hour) + ":" + to_string(ltm->tm_min) + ":" + to_string(ltm->tm_sec);
    if (inclureDate)
    {
        return date + " " + heure;
    }
    else
    {
        return heure;
    }
}

void ecrireBlog(int identite, type expediteur, string date)
{
    if (expediteur == EGOISTE)
    {
        mtx.lock();
        string msg = "ID: " + to_string(identite) + " Line: " + to_string(pd->actualle_ligne) + " Date: " + date;
        string donnees = "My name is " + to_string(identite) + ", I'm a selfish reader, the current hour is: " + obtenirTemps(false) + ". My message is: " + msg + "\n";
        cout<<donnees<<endl;
        enregistrerDesDonness(donnees, &(pd->fileptr));
        mtx.unlock();
    }
}

int attribuerIdentite()
{
    int id = pd->actuelIdentite;
    pd->actuelIdentite++;
    return id;
}

int attribuerMaLigneRandom()
{
    led.ma_ligne = ((rand() % pd->actualle_ligne) + 0);
    return led.ma_ligne;
}

int attribuerFauxIdentite()
{
    int id = led.faussesIdentites;
    led.faussesIdentites++;
    return id;
}

void *main_processus_Lecteur(void *args)
{
    processusGenerateur();

    while (true)
    {
        sem_wait(sempr);
        egoistePermitiur = true;
        etatFinalise = pd->finalise;
        sleep(1);
        egoistePermitiur = false;
        usleep(100000);
        sem_post(sempr);
        usleep(100000);
        if (etatFinalise)
            break;
    }

    return NULL;
}

void *processusLecteur(void *args)
{
    int identite = attribuerIdentite();
    int faux_identite = attribuerFauxIdentite();

    int maLigne = attribuerMaLigneRandom();

    cout << "My name is " << identite << endl;
    cout << "My fake name is " << faux_identite << endl;
    moi_etats->etats[faux_identite].processus_id = identite;

    while (true)
    {
        moi_etats->etats[faux_identite].processus_etat = BLOQUE;
        while (!egoistePermitiur && !etatFinalise)
            ;
        if (etatFinalise)
        {
            pthread_exit(NULL);
        }

        moi_etats->etats[faux_identite].processus_etat = ACTIF;

        maLigne = attribuerMaLigneRandom();

        if (letd.num_actualle_egoiste > 3)
        {
            cout << "\nATTENTION: There are more than 3 selfish readers!" << letd.num_actualle_egoiste << endl;
            letd.num_actualle_egoiste = 0;
            egoistePermitiur = false;
        }
        else
        {
            string date = obtenirTemps(true);
            letd.num_actualle_egoiste++;


            pd->donnees[maLigne].processus_identite = 0;
            pd->donnees[maLigne].ligne = 0;
            strcpy(pd->donnees[maLigne].date, "\0");

            ecrireBlog(identite, EGOISTE, date);

            sleep(led.temps_lu);

            if (letd.num_actualle_egoiste > 0)
                letd.num_actualle_egoiste--;

            moi_etats->etats[faux_identite].processus_etat = DORMI;
            sleep(led.temps_dormi);
        }
    }
    return NULL;
}

void processusGenerateur()
{
    for (int i = 0; i < led.quantite; i++)
    {
        cout << "\nProcess: " << i << endl;

        pthread_t thread_id;
        pthread_create(&thread_id, NULL, processusLecteur, NULL);
    }
}

void processusGenesis()
{
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, main_processus_Lecteur, NULL);
    pthread_join(thread_id, NULL);
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    int mli_id = shmget(PD_MLI, sizeof(maxlignes), IPC_CREAT | 0664);

    maxlignes = (int *)shmat(mli_id, NULL, 0);

    int taille = sizeof(struct partagees_donnees) + sizeof(struct ligne_donnees) * (*maxlignes);
    int shm_id = shmget(PD_CLE, taille, IPC_CREAT | 0664);
    pd = (partagees_donnees *)shmat(shm_id, NULL, 0);

    semsf = sem_open(SF_NAME, 0);
    sempr = sem_open(PR_NAME, 0);

    cout << "Selfish Reader \n";
    cout << "How many selfish readers?: ";
    cin >> led.quantite;

    cout << "Sleep time rate \n";
    cout << "How many seconds?: ";
    cin >> led.temps_dormi;

    cout << "Read time rate \n";
    cout << "How many seconds?: ";
    cin >> led.temps_lu;

    if (led.quantite <= 0 || led.temps_dormi <= 0 || led.temps_lu <= 0)
    {
        cout << "\nError in data\n";
        return -1;
    }

    int size = sizeof(executionEtats) + (sizeof(executionEtat) * (led.quantite));
    int EE_CLE = shmget(EGOISTE_ETATS, size, 0644 | IPC_CREAT);
    moi_etats = (executionEtats *)shmat(EE_CLE, NULL, 0);

    pd->quantiteEgoiste = led.quantite;
    pd->quantiteInitialisee++;

    processusGenesis();

    sem_wait(sempr);
    pd->quantiteTerminee++;
    sem_post(sempr);
    cout << "Selfish Reader Program Ended" << endl;
    return 0;
}