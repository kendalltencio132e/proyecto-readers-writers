#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h> 

#include <stdlib.h>
#include <fcntl.h>

#include <sys/stat.h>
#include <sys/wait.h>
#include "lesStructures.hpp"
#include <semaphore.h>

#define SEM_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define INITIAL_VALUE 1

using namespace std;

int main(int argc, char *argv[])
{
  
    if(argc<2){
        cout<<("insufficient arguments, ./initialiseur <maxlines>")<<endl;
        return -1;
    }else{
        cout<<"initialization"<<endl;
        remove("bitacora.txt");

        int *maxlignes;

        //Memoria compartida para maxlines
        int mli_id = shmget(PD_MLI, sizeof(maxlignes) , IPC_CREAT | 0664);
        if(mli_id<0){       
            perror("shmget(3) failed");
            return -1;
        }
        maxlignes =(int *)shmat(mli_id, NULL, 0);
        *maxlignes=atoi(argv[1]);

        //Memoria compartida para partagees_donneess
        int taille = sizeof(struct partagees_donnees)+sizeof(struct ligne_donnees) * (*maxlignes);
        cout<<"taille: "<<taille<<endl;

        int shm_id = shmget(PD_CLE, taille , IPC_CREAT | 0664);
        if(shm_id<0){       
            perror("shmget(3) failed");
            return -1;
        }
        partagees_donnees *pd = (partagees_donnees *)shmat(shm_id, NULL, 0);
        pd->quantiteInitialisee=0;
        pd->quantiteTerminee=0;
        pd->finalise=false;

        cout<<"shared memory created "<<endl;
        
        //Inicializacion de los semaforos
        sem_t *sempr =sem_open(SF_NAME, O_CREAT, 0644, 1);
        sem_t *semfr =sem_open(PR_NAME, O_CREAT, 0644, 1);

        if (sempr == SEM_FAILED || semfr == SEM_FAILED )  {
            perror("sem_open(3) error"); 
            exit(EXIT_FAILURE);
        }
        cout<<"Initialized data"<<endl;

        return 0;
    }
}





/*


#include <iostream>
using std::cerr;
using std::endl;
#include <fstream>
using std::ofstream;
#include <cstdlib> // for exit function
// This program output values from an array to a file named example2.dat
int main()
{
   ofstream outdata; // outdata is like cin
   int i; // loop index
   int num[5] = {4, 3, 6, 7, 12}; // list of output values

outdata.open("example2.dat"); // opens the file
   if( !outdata ) { // file couldn't be opened
      cerr << "Error: file could not be opened" << endl;
      exit(1);
   }


 
   return 0;
}


    //Write ON FILE
    int file= open(FILENAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    lseek(file, pd->fileptr, SEEK_CUR); //Mueve el puntero del archivo 12 espacios
    pd->fileptr+=write(file,"Buenos Dias\n", 12);
    close(file);


*/