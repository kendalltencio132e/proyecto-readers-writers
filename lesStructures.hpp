#include <bits/types/FILE.h>

#define PD_CLE 0x203
#define PD_MLI 0x204

#define ECRIVAIN_ETATS 0x205
#define LECTEUR_ETATS 0x206
#define EGOISTE_ETATS 0x207

#define SF_NAME "/mysem2_sf"
#define PR_NAME "/mysem2_pr"

#define FILENAME "bitacora.txt"

#include <semaphore.h>
#include <stdio.h>
#include <string>
#include <fcntl.h>
#include <ctime>

enum etatProcessus { NON, DORMI , BLOQUE, ACTIF};
enum type {ECRIVAIN, LECTEUR, EGOISTE};

struct executionEtat
{
    int processus_id = -1;
    etatProcessus processus_etat = NON;
};

struct executionEtats
{
    bool securite_bool;
    executionEtat etats[];
};

struct ligne_donnees
{
    int processus_identite;
    int ligne;
    char date[18];
};

struct partagees_donnees
{
    int actuelIdentite;
    int quantiteTerminee;    //Cantidad de finalizados
    int quantiteInitialisee; //Cantidad de iniciados
    int quantiteEgoiste;
    int quantiteLecteur;
    int quantiteEcrivain;
    bool finalise;
    bool plein;
    int actualle_ligne;
    int fileptr;
    ligne_donnees donnees[];
};

