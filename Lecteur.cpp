/* Fichier: Lecteur.cpp Auteur: Kendall Tencio */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <list>
#include <semaphore.h>
#include <mutex>
#include "lesStructures.hpp"

using namespace std;

struct lecteur_thread_donnees
{
    int pidNouveau;
};

struct lecteur_donnees
{
    int quantite;
    int temps_dormi;
    int temps_lu;
    int ma_ligne;
    int faussesIdentites;
};

executionEtats *moi_etats;
partagees_donnees *pd;
lecteur_donnees ld;
list<pthread_t> listeFils;
bool etatFinalise = false;

int *maxlignes;
sem_t *semsf;
sem_t *sempr;
mutex mtx;
mutex mtx_cout;

void processusGenerateur();

bool lecteurPermitiur;

void enregistrerDesDonness(string donness, int *seek)
{
    int file = open(FILENAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    lseek(file, *seek, SEEK_CUR);
    *seek += write(file, &(donness[0]), donness.length());
    close(file);
}

string obtenirTemps(bool inclureDate)
{
    time_t now = time(0);
    tm *ltm = localtime(&now);
    string date = to_string(ltm->tm_mday) + "/" + to_string(1 + ltm->tm_mon) + "/" + to_string(1900 + ltm->tm_year);
    string heure = to_string(ltm->tm_hour) + ":" + to_string(ltm->tm_min) + ":" + to_string(ltm->tm_sec);
    if (inclureDate)
    {
        return date + " " + heure;
    }
    else
    {
        return heure;
    }
}

void ecrireBlog(int identite, type expediteur, string date)
{
    if (expediteur == LECTEUR)
    {
        mtx.lock();
        string msg = "ID: " + to_string(identite) + " Line: " + to_string(pd->actualle_ligne) + " Date: " + date;
        string donnees = "My name is " + to_string(identite) + ", I'm a reader, the current hour is: " + obtenirTemps(false) + ". My message is: " + msg + "\n";
        cout<<donnees<<endl;
        enregistrerDesDonness(donnees, &(pd->fileptr));
        mtx.unlock();
    }
}

int attribuerIdentite()
{
    int id = pd->actuelIdentite;
    pd->actuelIdentite++;
    return id;
}

int attribuerMaLigne()
{
    ld.ma_ligne = 0;
    return ld.ma_ligne;
}

int attribuerFauxIdentite()
{
    int id = ld.faussesIdentites;
    ld.faussesIdentites++;
    return id;
}

void *main_processus_Lecteur(void *args)
{
    processusGenerateur();
    bool etatFinalise = false;
    while (true)
    {
        sem_wait(sempr);
        etatFinalise = pd->finalise;
        lecteurPermitiur = true;
        sleep(1);
        lecteurPermitiur = false;
        usleep(100000);
        sem_post(sempr);
        usleep(100000);
        if (etatFinalise)
            break;
    }

    pthread_exit(NULL);
}

void *processusLecteur(void *args)
{
    int identite = attribuerIdentite();
    int faux_identite = attribuerFauxIdentite();

    int maLigne = attribuerMaLigne();

    cout << "My name is " << identite << endl;
    cout << "My fake name is " << faux_identite << endl;

    moi_etats->etats[faux_identite].processus_id = identite;
    bool etatFinalise = false;
    while (true)
    {
        moi_etats->etats[faux_identite].processus_etat = BLOQUE;
        while (!lecteurPermitiur && !etatFinalise)
            ;
        if (etatFinalise)
        {
            pthread_exit(NULL);
        }

        moi_etats->etats[faux_identite].processus_etat = ACTIF;

        if (maLigne == pd->actualle_ligne)
        {
            maLigne = attribuerMaLigne();
            cout << "\nGoing to the beginning of the written lines!!!\n"
                 << endl;
        }
        string date = obtenirTemps(true);

        mtx_cout.lock();
       
        if (pd->donnees[maLigne].processus_identite == 0 && pd->donnees[maLigne].ligne == 0 && maLigne != 0)
            cout << "A selfish reader stole this line!" << endl << endl;

        etatFinalise = pd->finalise;
        mtx_cout.unlock();

        ecrireBlog(identite, LECTEUR, date);

        sleep(ld.temps_lu);

        maLigne++;
        moi_etats->etats[faux_identite].processus_etat = DORMI;
        sleep(ld.temps_dormi);
    }
}

void processusGenerateur()
{
    for (int i = 0; i < ld.quantite; i++)
    {
        cout << "\nProcess: " << i << endl;

        pthread_t thread_id;
        listeFils.push_back(thread_id);
        pthread_create(&thread_id, NULL, processusLecteur, NULL);
    }
}

void processusGenesis()
{
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, main_processus_Lecteur, NULL);
    pthread_join(thread_id, NULL);
}

int main(int argc, char *argv[])
{
    int mli_id = shmget(PD_MLI, sizeof(maxlignes), IPC_CREAT | 0664);

    maxlignes = (int *)shmat(mli_id, NULL, 0);

    int taille = sizeof(struct partagees_donnees) + sizeof(struct ligne_donnees) * (*maxlignes);
    int shm_id = shmget(PD_CLE, taille, IPC_CREAT | 0664);
    pd = (partagees_donnees *)shmat(shm_id, NULL, 0);

    semsf = sem_open(SF_NAME, 0);
    sempr = sem_open(PR_NAME, 0);

    cout << "Reader \n";
    cout << "How many readers?: ";
    cin >> ld.quantite;

    cout << "Sleep time rate \n";
    cout << "How many seconds?: ";
    cin >> ld.temps_dormi;

    cout << "Read time rate \n";
    cout << "How many seconds?: ";
    cin >> ld.temps_lu;

    if (ld.quantite <= 0 || ld.temps_dormi <= 0 || ld.temps_lu <= 0)
    {
        cout << "\nError in data\n";
        return -1;
    }

    int size = sizeof(executionEtats) + (sizeof(executionEtat) * (ld.quantite));
    int EE_CLE = shmget(LECTEUR_ETATS, size, 0644 | IPC_CREAT);
    moi_etats = (executionEtats *)shmat(EE_CLE, NULL, 0);

    pd->quantiteLecteur = ld.quantite;
    pd->quantiteInitialisee++;

    processusGenesis();

    sem_wait(sempr);
    pd->quantiteTerminee++;
    sem_post(sempr);
    cout << "Reader Program Ended" << endl;
    return 0;
}
