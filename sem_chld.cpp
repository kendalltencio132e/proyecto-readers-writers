#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
using namespace std;

#define SEM_NAME "/semaphore_example"
#define ITERS 10

int maxlignes;

int main(int argc, char *argv[])
{
    maxlignes=atoi(argv[1]);
    int pd_id=atoi(argv[2]);
    char* sem_name=argv[3];

    cout<<maxlignes<<" "<<pd_id<<" "<< sem_name<<endl;
    
    sem_t *semaphore = sem_open(sem_name, O_RDWR);
    if (semaphore == SEM_FAILED) {
        perror("sem_open(3) failed");
        exit(EXIT_FAILURE);
    }

    int i;
    for (i = 0; i < ITERS; i++) {
        if (sem_wait(semaphore) < 0) {
            perror("sem_wait(3) failed on child");
            continue;
        }

        printf("PID %ld acquired semaphore\n", (long) getpid());

        if (sem_post(semaphore) < 0) {
            perror("sem_post(3) error on child");
        }

        sleep(1);
    }

    if (sem_close(semaphore) < 0)
        perror("sem_close(3) failed");

    return 0;
}
