#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <errno.h>
#include <list>
#include <mutex>
#include "lesStructures.hpp"
#include <pthread.h>

using namespace std;

executionEtats *moi_etatsEcrivain;
int ecrivains;
executionEtats *moi_etatsEgoiste;
int egoistes;
executionEtats *moi_etatsLecteur;
int lecteures;
sem_t *sempr; 
partagees_donnees *pd;
int *maxlignes;
bool operatoire=false;
bool forcedFinalise=false;
void etatDuFichier(){
    cout<<"\nState of file"<<endl;
    cout<<"n\t"<<"pid\t"<<"ligne\t"<<"date"<<endl;
    for(int n = 0; n < *maxlignes;n++){
        cout<<n<<"\t";
        cout<<pd->donnees[n].processus_identite<<"\t";
        cout<<pd->donnees[n].ligne<<"\t";
        cout<<pd->donnees[n].date<<endl;
    }
}
string caseEtatProcess(etatProcessus etat){
    switch(etat){
        case(etatProcessus::NON):
            return "NON";
            break;
        case(etatProcessus::DORMI):
            return "DORMI";
            break;
        case(etatProcessus::BLOQUE):
            return "BLOQUE";
            break;
        case(etatProcessus::ACTIF):
            return "ACTIF";
            break;
        default:
            return "NULL";
    }


}
void etatsDesEcrivains(){
    cout<<"\nStates of writers"<<endl;
    cout<<"n\t"<<"pid\t"<<"state\t"<<endl;
    for(int n = 0; n < ecrivains;n++){
        cout<<n<<"\t";
        cout<<moi_etatsEcrivain->etats[n].processus_id<<"\t";
        cout<< caseEtatProcess(moi_etatsEcrivain->etats[n].processus_etat)<<endl ;
    }
}
void etatsDesLecteurs(){
    cout<<"\nStates of readers"<<endl;
    cout<<"n\t"<<"pid\t"<<"etat\t"<<endl;
    for(int n = 0; n < lecteures;n++){
        cout<<n<<"\t";
        cout<<moi_etatsLecteur->etats[n].processus_id<<"\t";
        cout<< caseEtatProcess(moi_etatsLecteur->etats[n].processus_etat)<<endl ;
    }
}
void etatsDesEgoistes(){
    cout<<"\nStates of egoists"<<endl;
    cout<<"n\t"<<"pid\t"<<"etat\t"<<endl;
    for(int n = 0; n < egoistes;n++){
        cout<<n<<"\t";
        cout<<moi_etatsEgoiste->etats[n].processus_id<<"\t";
        cout<< caseEtatProcess(moi_etatsEgoiste->etats[n].processus_etat)<<endl ;
    }
}

void *menu(void *args) {
	int val, opt;
	while (true) {
		cout << "\n\n1. State of file\n";//Mostrar todas las lineas
		cout << "2. State of writers\n";//Estado de los writers
		cout << "3. State of readers\n";//Estado de los Readers
		cout << "4. State of egoists\n"; //Estado de los egoistas
		cout << "5. Leave \n Option? ";
        operatoire=false;
		cin >> opt;
		cout << endl;
        operatoire=true;
		switch (opt) {
		case 1:
			etatDuFichier();
			break;
		case 2:
			etatsDesEcrivains();
			break;
		case 3:
			etatsDesLecteurs();
			break;
		case 4:
			etatsDesEgoistes();
			break;
		case 5:
            forcedFinalise=true;
			return 0;
		}
		cout << endl;
	}
	return 0;
}

int main(){
        
        int mli_id = shmget(PD_MLI, sizeof(maxlignes), IPC_CREAT | 0664);
        maxlignes = (int *)shmat(mli_id, NULL, 0);

        int taille = sizeof(struct partagees_donnees) + sizeof(struct ligne_donnees) * (*maxlignes);
        int shm_id = shmget(PD_CLE, taille, IPC_CREAT | 0664);
        pd = (partagees_donnees *)shmat(shm_id, NULL, 0);

        ecrivains = pd->quantiteEcrivain;
        egoistes = pd->quantiteEgoiste;
        lecteures = pd->quantiteLecteur;

        sempr = sem_open(PR_NAME, 0);

        //Lecteur
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteLecteur));
        int ltr_id = shmget(LECTEUR_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsLecteur = (executionEtats *)shmat(ltr_id, NULL, 0);

        //Egoiste
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteEgoiste));
        int ege_id = shmget(EGOISTE_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsEgoiste = (executionEtats *)shmat(ege_id, NULL, 0);

        //Ecrivain
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteEcrivain));
        int ecn_id = shmget(ECRIVAIN_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsEcrivain = (executionEtats *)shmat(ecn_id, NULL, 0);


        pthread_t thread_id;
        pthread_create(&thread_id, NULL, menu, NULL);

        pd->quantiteInitialisee++;

        while((!pd->finalise || operatoire) && !forcedFinalise);
        pthread_cancel(thread_id);

        sem_wait(sempr);
        pd->quantiteTerminee++;
        sem_post(sempr);
        return 0;
}