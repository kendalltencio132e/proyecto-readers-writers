#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>

#include <stdlib.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "lesStructures.hpp"

#define SEM_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define INITIAL_VALUE 1

using namespace std;

int main(int argc, char *argv[])
{
    int *maxlignes;
    int mli_id = shmget(PD_MLI, sizeof(maxlignes), IPC_CREAT | 0664);
    if (mli_id < 0)
    {
        perror("shmget(3) failed");
        return -1;
    }
    maxlignes = (int *)shmat(mli_id, NULL, 0);
    if (*maxlignes <= 0)
    {
        shmdt(maxlignes);
        shmctl(mli_id, IPC_RMID, 0);
        cout << "Error: Data didn't initialize" << endl;
        return -1;
    }
    else
    {
        int taille = sizeof(struct partagees_donnees) + sizeof(struct ligne_donnees) * (*maxlignes);
        int shm_id = shmget(PD_CLE, taille, IPC_CREAT | 0664);
        partagees_donnees *pd = (partagees_donnees *)shmat(shm_id, NULL, 0);

        sem_t *semsf = sem_open(SF_NAME, 0);
        sem_t *sempr = sem_open(PR_NAME, 0);

        executionEtats *moi_etatsEcrivain;
        executionEtats *moi_etatsEgoiste;
        executionEtats *moi_etatsLecteur;

        //Lecteur
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteLecteur));
        int ltr_id = shmget(LECTEUR_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsLecteur = (executionEtats *)shmat(ltr_id, NULL, 0);

        //Egoiste
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteEgoiste));
        int ege_id = shmget(EGOISTE_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsEgoiste = (executionEtats *)shmat(ege_id, NULL, 0);

        //Ecrivain
        taille = sizeof(executionEtats) + (sizeof(executionEtat) * (pd->quantiteEcrivain));
        int ecn_id = shmget(ECRIVAIN_ETATS, taille, 0644 | IPC_CREAT);
        moi_etatsEcrivain = (executionEtats *)shmat(ecn_id, NULL, 0);

        cout << "FINISEUR" << endl;

        int quantiteTerminee = 0;    //Cantidad de finalizados
        int quantiteInitialisee = 0; //Cantidad de iniciados

        if (pd->quantiteInitialisee != pd->quantiteTerminee)
        {
            do
            {
                sem_wait(sempr);

                if (!pd->finalise)
                    pd->finalise = true;

                quantiteTerminee = pd->quantiteTerminee;
                quantiteInitialisee = pd->quantiteInitialisee;

                sem_post(sempr);

                sleep(1);
            } while (quantiteTerminee != quantiteInitialisee);

        sem_close(sempr);
        sem_close(semsf);

        sem_unlink(SF_NAME);
        sem_unlink(PR_NAME);

        shmdt(maxlignes);
        shmctl(mli_id, IPC_RMID, 0);

        shmdt(pd);
        shmctl(shm_id, IPC_RMID, 0);

        shmdt(moi_etatsLecteur);
        shmctl(ltr_id, IPC_RMID, 0);

        shmdt(moi_etatsEgoiste);
        shmctl(ege_id, IPC_RMID, 0);

        shmdt(moi_etatsEcrivain);
        shmctl(ecn_id, IPC_RMID, 0);
        
        cout << "All data was destroyed" << endl;

        return 0;
        }
    }
}

