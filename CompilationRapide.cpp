
#include <iostream>
using namespace std;
#include <string.h>

#define LONGUEUR 7

void executer(string command)
{
    system(&(command[0]));
}
void executerToutes(string commandes[])
{
    for (int i = 0; i < LONGUEUR; i++)
        executer(commandes[i]);
}
void executerLeCas(char *lecase, string commandes[])
{
    if (strcmp("initialiseur", lecase) == 0)
        executer(commandes[0]);
    else if (strcmp("ecrivain", lecase) == 0)
        executer(commandes[1]);
    else if (strcmp("lecteur", lecase) == 0)
        executer(commandes[2]);
    else if (strcmp("compiler", lecase) == 0)
        executer(commandes[3]);
    else if (strcmp("finisseur", lecase) == 0)
        executer(commandes[4]);
    else if (strcmp("egoiste", lecase) == 0)
        executer(commandes[5]);
    else if (strcmp("espion", lecase) == 0)
        executer(commandes[6]);
    else if (strcmp("toutes", lecase) == 0)
        executerToutes(commandes);
}

int main(int argc, char *argv[])
{
    string commandes[LONGUEUR] = {"g++ -pthread -o initialiseur Initialiseur.cpp",
                                  "g++ -pthread -o ecrivain Ecrivain.cpp",
                                  "g++ -pthread -o lecteur Lecteur.cpp",
                                  "g++ -pthread -o compiler CompilationRapide.cpp",
                                  "g++ -pthread -o finisseur Finisseur.cpp",
                                  "g++ -pthread -o egoiste LecteurEgoiste.cpp",
                                  "g++ -pthread -o espion Espion.cpp"};

    if (argc > 1)
    {
        for (int c = 1; c < argc; c++)
            executerLeCas(argv[c], commandes);
    }
    else
    {
        executerToutes(commandes);
    }
    return 0;
}
