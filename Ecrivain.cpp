/* Fichier: Ecrivain.cpp Auteur: David Salazar */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <semaphore.h>
#include <cstdlib>
#include "lesStructures.hpp"
#include <stdlib.h>
#include <fcntl.h>

#include <stdio.h>
#include <string.h>
using namespace std;

struct ecrivains_donnees
{
    int quantite;
    int temps_dormi;
    int temps_ecriture;
    int faussesIdentites;
};

ecrivains_donnees ed;
executionEtats *moi_etats;

struct partagees_donnees *pd;
int *maxlignes;
sem_t *semsf;
sem_t *sempr;

int threadsDemarresiles;

void enregistrerDesDonness(string donness, int *seek)
{
    int file = open(FILENAME, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    lseek(file, *seek, SEEK_CUR);
    *seek += write(file, &(donness[0]), donness.length());
    close(file);
}

string obtenirTemps(bool inclureDate)
{
    time_t now = time(0);
    tm *ltm = localtime(&now);
    string date = to_string(ltm->tm_mday) + "/" + to_string(1 + ltm->tm_mon) + "/" + to_string(1900 + ltm->tm_year);
    string heure = to_string(ltm->tm_hour) + ":" + to_string(ltm->tm_min) + ":" + to_string(ltm->tm_sec);
    if (inclureDate)
    {
        return date + " " + heure;
    }
    else
    {
        return heure;
    }
}

void ecrireBlog(int identite, type expediteur, string date)
{
    if (expediteur == ECRIVAIN)
    {
        string msg = "ID: " + to_string(identite) + " Line: " + to_string(pd->actualle_ligne) + " Date: " + date;
        string donnees = "My ID is " + to_string(identite) + ", I am a writer. The actual time is: " + obtenirTemps(false) + ". The message I wrote is: " + msg + "\n";
        cout<<donnees<<endl;
        enregistrerDesDonness(donnees, &(pd->fileptr));
    }
}

int attribuerIdentite()
{
    int id = pd->actuelIdentite;
    pd->actuelIdentite++;
    return id;
}

int attribuerFauxIdentite()
{
    int id = ed.faussesIdentites;
    ed.faussesIdentites++;
    return id;
}

void *ecrireDonnees(void *args)
{
    int identite = attribuerIdentite();
    int faux_identite = attribuerFauxIdentite();

    cout << "My ID is: " << identite << endl;
    cout << "My fake ID is: " << faux_identite << endl
         << endl;
    moi_etats->etats[faux_identite].processus_id = identite;

    while (true)
    {
        moi_etats->etats[faux_identite].processus_etat = BLOQUE;
        sem_wait(sempr);
        if(pd->finalise) {threadsDemarresiles--;sem_post(sempr);break;}

        moi_etats->etats[faux_identite].processus_etat = ACTIF;

        if (!(pd->plein))
        {
            string date = obtenirTemps(true);
            pd->donnees[pd->actualle_ligne].processus_identite = identite;
            pd->donnees[pd->actualle_ligne].ligne = pd->actualle_ligne;
            strcpy (pd->donnees[pd->actualle_ligne].date,&(date[0]));
          

            ecrireBlog(identite, ECRIVAIN, date);
            

            pd->actualle_ligne++;
            if (pd->actualle_ligne == (*maxlignes))
            {
                cout << "File is full" << endl;
                pd->plein = true;
            }
            sleep(ed.temps_ecriture);
        }

        sem_post(sempr);
        moi_etats->etats[faux_identite].processus_etat = DORMI;
        sleep(ed.temps_dormi);
    }
    return NULL;
}

void creerProcessus()
{
    for (int i = 0; i < ed.quantite; i++)
    {
        pthread_t thread_id;
        threadsDemarresiles ++;
        pthread_create(&thread_id, NULL, ecrireDonnees, NULL);
    }
}

int main(int argc, char *argv[])
{
    int mli_id = shmget(PD_MLI, sizeof(maxlignes), IPC_CREAT | 0664);

    maxlignes = (int *)shmat(mli_id, NULL, 0);

    int taille = sizeof(struct partagees_donnees) + sizeof(struct ligne_donnees) * (*maxlignes);
    int shm_id = shmget(PD_CLE, taille, IPC_CREAT | 0664);
    pd = (partagees_donnees *)shmat(shm_id, NULL, 0);

    semsf = sem_open(SF_NAME, 0);
    sempr = sem_open(PR_NAME, 0);

    cout << "Writers \n";
    cout << "Introduce the quantity: ";
    cin >> ed.quantite;

    cout << "Time sleeping \n";
    cout << "Introduce the duration: ";
    cin >> ed.temps_dormi;

    cout << "Time writing \n";
    cout << "Introduce the duration: ";
    cin >> ed.temps_ecriture;

    if (ed.quantite <= 0 || ed.temps_dormi <= 0 || ed.temps_ecriture <= 0)
    {
        cout << "\nWrong data\n";
        return -1;
    }

    cout << "Quantity of writers: " << ed.quantite << "\n";
    cout << "Sleep duration: " << ed.temps_dormi << "\n";
    cout << "Write duration: " << ed.temps_ecriture << "\n"
         << endl;

    int size = sizeof(executionEtats) + (sizeof(executionEtat) * (ed.quantite));
    int EE_CLE = shmget(ECRIVAIN_ETATS, size, 0644 | IPC_CREAT);

    moi_etats = (executionEtats *)shmat(EE_CLE, NULL, 0);
    
    pd->quantiteEcrivain=ed.quantite;
    pd->quantiteInitialisee++;


    creerProcessus();
    while(threadsDemarresiles>0);

    sem_wait(sempr);
    pd->quantiteTerminee++;
    sem_post(sempr);

    cout<<"Writer is over"<<endl;

    return 0;
}
